-- News table

create table news_post
(
  id             serial primary key,
  author_id      int not null references account (id),
  headline       varchar(255) not null,
  body           text not null,
  tags           varchar(255),
  pinned         boolean NOT NULL DEFAULT false ,
  create_time    timestamp NOT NULL DEFAULT current_timestamp,
  update_time    timestamp NOT NULL DEFAULT current_timestamp
);

CREATE TRIGGER news_create_time
  BEFORE INSERT
  ON news_post
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER news_update_time
  BEFORE UPDATE
  ON news_post
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();