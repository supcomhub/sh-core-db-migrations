-- DON'T EVEN THINK OF UPDATING THIS FILE
-- DON'T EVEN THINK OF UPDATING THIS FILE
-- DON'T EVEN THINK OF UPDATING THIS FILE
-- DON'T EVEN THINK OF UPDATING THIS FILE
-- DON'T EVEN THINK OF UPDATING THIS FILE
-- DON'T EVEN THINK OF UPDATING THIS FILE
-- DON'T EVEN THINK OF UPDATING THIS FILE
-- DON'T EVEN THINK OF UPDATING THIS FILE
-- DON'T EVEN THINK OF UPDATING THIS FILE
-- DON'T EVEN THINK OF UPDATING THIS FILE
-- DON'T EVEN THINK OF UPDATING THIS FILE
-- DON'T EVEN THINK OF UPDATING THIS FILE
-- DON'T EVEN THINK OF UPDATING THIS FILE
-- DON'T EVEN THINK OF UPDATING THIS FILE
-- DON'T EVEN THINK OF UPDATING THIS FILE
-- DON'T EVEN THINK OF UPDATING THIS FILE
-- DON'T EVEN THINK OF UPDATING THIS FILE
-- DON'T EVEN THINK OF UPDATING THIS FILE
-- DON'T EVEN THINK OF UPDATING THIS FILE
-- DON'T EVEN THINK OF UPDATING THIS FILE

-- Extensions

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- Common functions

create or replace function update_update_time()
  returns trigger AS
$$
begin
  NEW.update_time = now();
  return NEW;
end;
$$
  language plpgsql;

-- Hibernate tries to insert null into create_time and update_time, this function replaces it before insert
create or replace function set_insert_times()
  returns trigger AS
$$
begin
  NEW.create_time = now();
  NEW.update_time = now();
  return NEW;
end;
$$
  language plpgsql;

CREATE OR REPLACE FUNCTION f_if(boolean, anyelement, anyelement)
  RETURNS anyelement AS
$$
BEGIN
  CASE WHEN ($1)
    THEN
      RETURN ($2);
    ELSE
      RETURN ($3);
    END CASE;
  EXCEPTION
  WHEN division_by_zero
    THEN
      RETURN ($3);
END;
$$
  LANGUAGE plpgsql;

-- Terms of Service

create table terms_of_service
(
  id          serial primary key,
  content     text      not null,
  published   boolean   not null default false,
  valid_from  timestamp not null,
  create_time timestamp NOT NULL DEFAULT current_timestamp,
  update_time timestamp NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER terms_of_service_create_time
  BEFORE INSERT
  ON terms_of_service
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER terms_of_service_update_time
  BEFORE UPDATE
  ON terms_of_service
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

-- Account

create table account
(
  id                    serial primary key,
  display_name          varchar(255)                         not null,
  password              varchar(255)                         null,
  last_agreed_tos_id    int references terms_of_service (id) null,
  last_login_user_agent varchar(255)                         null,
  last_login_ip_address varchar(255)                         null,
  last_login_time       timestamp                            null,
  create_time           timestamp                            NOT NULL DEFAULT current_timestamp,
  update_time           timestamp                            NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER account_create_time
  BEFORE INSERT
  ON account
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER account_update_timespan
  BEFORE UPDATE
  ON account
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

CREATE unique index on account (lower(display_name));

-- Account verification
create type account_verification_type as enum ('STEAM', 'TELEGRAM', 'PERSONAL');

create table account_verification
(
  id          serial primary key,
  account_id  int references account (id),
  type        account_verification_type,
  value       varchar(255) not null,
  create_time timestamp    NOT NULL DEFAULT current_timestamp,
  update_time timestamp    NOT NULL DEFAULT current_timestamp
);
create unique index on account_verification (account_id, type);
CREATE TRIGGER account_verification_create_time
  BEFORE INSERT
  ON account_verification
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER account_verification_update_time
  BEFORE UPDATE
  ON account_verification
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

-- Avatar

create table avatar
(
  id          serial primary key,
  url         varchar(2000) not null unique,
  description varchar(255)  null unique,
  create_time timestamp     NOT NULL DEFAULT current_timestamp,
  update_time timestamp     NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER avatar_create_time
  BEFORE INSERT
  ON avatar
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER avatar_update_time
  BEFORE UPDATE
  ON avatar
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

create table avatar_assignment
(
  id          serial PRIMARY KEY,
  account_id  int references account (id) not null,
  avatar_id   int references avatar (id)  not null,
  selected    boolean                              default false not null,
  expiry_time timestamp                   null,
  create_time timestamp                   NOT NULL DEFAULT current_timestamp,
  update_time timestamp                   NOT NULL DEFAULT current_timestamp
);
create unique index on avatar_assignment (account_id, avatar_id);
CREATE TRIGGER avatar_assignment_create_time
  BEFORE INSERT
  ON avatar_assignment
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER avatar_assignment_update_time
  BEFORE UPDATE
  ON avatar_assignment
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

-- Achievement

CREATE TYPE achievement_type AS ENUM ('STANDARD', 'INCREMENTAL');
CREATE TYPE achievement_state AS ENUM ('HIDDEN', 'REVEALED', 'UNLOCKED');
CREATE TABLE achievement_definition
(
  id                uuid primary key,
  ordinal           int               NOT NULL,
  name_key          varchar(255)      NOT NULL,
  description_key   varchar(255)      NOT NULL,
  type              achievement_type  NOT NULL,
  total_steps       int,
  revealed_icon_url varchar(2000),
  unlocked_icon_url varchar(2000),
  initial_state     achievement_state NOT NULL,
  experience_points int               NOT NULL,
  create_time       timestamp         NOT NULL DEFAULT current_timestamp,
  update_time       timestamp         NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER achievement_definition_create_time
  BEFORE INSERT
  ON achievement_definition
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER achievement_definition_update_time
  BEFORE UPDATE
  ON achievement_definition
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

CREATE UNIQUE INDEX achievement_definition_name_key_uindex
  ON achievement_definition (lower(name_key));
CREATE UNIQUE INDEX achievement_definition_description_key_uindex
  ON achievement_definition (lower(description_key));
COMMENT ON COLUMN achievement_definition.ordinal
  IS 'The order in which the achievement is displayed to the user.';
COMMENT ON COLUMN achievement_definition.name_key
  IS 'The message key for the name of the achievement.';
COMMENT ON COLUMN achievement_definition.description_key
  IS 'The message key for the description of the achievement.';
COMMENT ON COLUMN achievement_definition.type
  IS 'The type of the achievement
Possible values are:
STANDARD - Achievement is either locked or unlocked
INCREMENTAL - Achievement takes a certain amount of steps before being unlocked.';
COMMENT ON COLUMN achievement_definition.total_steps
  IS 'The total steps for an incremental achievement, NULL for standard achievements.';
COMMENT ON COLUMN achievement_definition.revealed_icon_url
  IS 'The image URL for the revealed achievement icon.';
COMMENT ON COLUMN achievement_definition.unlocked_icon_url
  IS 'The image URL for the unlocked achievement icon.';
COMMENT ON column achievement_definition.initial_state
  IS 'The initial state of the achievement. \nPossible values are:\n"HIDDEN" - Achievement is hidden.\n"REVEALED" - Achievement is revealed.\n"UNLOCKED" - Achievement is unlocked.';
COMMENT ON COLUMN achievement_definition.experience_points
  IS 'Experience points which will be earned when unlocking this achievement. Multiple of 5. Reference:\n5 - Easy to achieve\n20 - Medium\n50 - Hard to achieve';

-- Email

create table email_address
(
  id          serial primary key,
  account_id  int          not null references account (id),
  plain       varchar(255) not null,
  distilled   varchar(255) not null,
  "primary"   boolean      not null,
  create_time timestamp    NOT NULL DEFAULT current_timestamp,
  update_time timestamp    NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER email_address_create_time
  BEFORE INSERT
  ON email_address
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER email_address_update_time
  BEFORE UPDATE
  ON email_address
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

comment on column email_address.distilled
  is 'Even though email addresses are different, the can belong to the same email account.
For instance ab@gmail.com, AB@gmail.com, a.b@gmail.com and ab+c@gmail.com all resolve
to the same email account. In this case, ''ab@gmail.com'' would be stored.';

comment on column email_address.account_id
  is 'The account this email address belongs to';

create unique index on email_address (lower(plain));
create index on email_address (lower(distilled));

alter table email_address
  add constraint primary_email_address unique (account_id, "primary")
    deferrable initially deferred;

-- Clan

CREATE TABLE clan
(
  id          serial PRIMARY KEY,
  name        varchar(40)                 NOT NULL,
  tag         varchar(3)                  NOT NULL,
  founder_id  int references account (id) NOT NULL,
  leader_id   int references account (id),
  description text,
  create_time timestamp                   NOT NULL DEFAULT current_timestamp,
  update_time timestamp                   NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER clan_create_time
  BEFORE INSERT
  ON clan
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER clan_update_time
  BEFORE UPDATE
  ON clan
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

CREATE UNIQUE INDEX ON clan (lower(tag));
CREATE UNIQUE INDEX ON clan (lower(name));

CREATE TABLE clan_membership
(
  id          serial PRIMARY KEY,
  clan_id     int       NOT NULL references clan (id),
  member_id   int       NOT NULL references account (id),
  create_time timestamp NOT NULL DEFAULT current_timestamp,
  update_time timestamp NOT NULL DEFAULT current_timestamp
);

CREATE TRIGGER clan_membership_create_time
  BEFORE INSERT
  ON clan_membership
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER clan_membership_update_time
  BEFORE UPDATE
  ON clan_membership
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

-- Email Domain Blacklist

CREATE TABLE email_domain_ban
(
  id          serial PRIMARY KEY,
  domain      varchar(255) NOT NULL,
  create_time timestamp    NOT NULL DEFAULT current_timestamp,
  update_time timestamp    NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER email_domain_ban_create_time
  BEFORE INSERT
  ON email_domain_ban
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER email_domain_ban_update_time
  BEFORE UPDATE
  ON email_domain_ban
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

CREATE UNIQUE INDEX email_domain_ban_email_domain
  ON email_domain_ban (lower(domain));

-- Event

CREATE TYPE event_type AS ENUM ('NUMERIC', 'TIME');
CREATE TABLE event_definition
(
  id          uuid primary key,
  name_key    varchar(255) NOT NULL,
  image_file  varchar(255),
  type        event_type   NOT NULL,
  create_time timestamp    NOT NULL DEFAULT current_timestamp,
  update_time timestamp    NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER event_definition_create_time
  BEFORE INSERT
  ON event_definition
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER event_definition_update_time
  BEFORE UPDATE
  ON event_definition
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

CREATE UNIQUE INDEX event_definition_name_key_uindex
  ON event_definition (lower(name_key));

-- Social relation

CREATE TYPE social_relation_type AS ENUM ('FRIEND', 'FOE');

create table social_relation
(
  id          serial primary key,
  from_id     int                  not null references account (id),
  to_id       int                  not null references account (id),
  relation    social_relation_type not null,
  create_time timestamp            NOT NULL DEFAULT current_timestamp,
  update_time timestamp            NOT NULL DEFAULT current_timestamp
);

CREATE TRIGGER social_relation_create_time
  BEFORE INSERT
  ON social_relation
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER social_relation_update_time
  BEFORE UPDATE
  ON social_relation
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

-- Featured mod

create table featured_mod
(
  id                 serial primary key,
  short_name         varchar(50)  not null,
  description        text         not null,
  display_name       varchar(255) not null,
  public             boolean      not null,
  ordinal            smallint     not null,
  git_url            varchar(2000),
  git_branch         varchar(255),
  current_version    int          not null,
  deployment_webhook text         null,
  allow_override     boolean      not null default false,
  create_time        timestamp    NOT NULL DEFAULT current_timestamp,
  update_time        timestamp    NOT NULL DEFAULT current_timestamp
);
COMMENT ON COLUMN featured_mod.deployment_webhook
  IS 'A webhook to be called after successful deployment.';
CREATE TRIGGER featured_mod_create_time
  BEFORE INSERT
  ON featured_mod
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER featured_mod_update_time
  BEFORE UPDATE
  ON featured_mod
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

create unique index featured_mod_short_name_uindex
  on featured_mod (lower(short_name));

create unique index featured_mod_display_name_uindex
  on featured_mod (lower(display_name));

-- Permission

create table group_permission
(
  id             serial primary key,
  technical_name varchar(255) not null,
  name_key       varchar(255) not null,
  create_time    timestamp    NOT NULL DEFAULT current_timestamp,
  update_time    timestamp    NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER group_permission_create_time
  BEFORE INSERT
  ON group_permission
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER group_permission_update_time
  BEFORE UPDATE
  ON group_permission
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

create table user_group
(
  id              serial primary key,
  technical_name  varchar(255) not null,
  name_key        varchar(255) not null,
  parent_group_id int references user_group (id),
  public          boolean      not null,
  create_time     timestamp    NOT NULL DEFAULT current_timestamp,
  update_time     timestamp    NOT NULL DEFAULT current_timestamp
);

CREATE TRIGGER user_group_create_time
  BEFORE INSERT
  ON user_group
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER user_group_update_time
  BEFORE UPDATE
  ON user_group
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

comment on table user_group
  is 'List of all user groups. Some of them are only informative (i.e. Council of Setons on website), others for permissions system.';
comment on column user_group.public
  is 'Public groups are visible for everyone, the rest only for internal permissions';


create table user_group_membership
(
  id          serial primary key,
  member_id   int       not null references account (id),
  group_id    int       not null references user_group (id),
  create_time timestamp NOT NULL DEFAULT current_timestamp,
  update_time timestamp NOT NULL DEFAULT current_timestamp
);
comment on table user_group_membership
  is 'Assign users (account) to a role (a bunch of permissions)';

CREATE TRIGGER user_group_membership_create_time
  BEFORE INSERT
  ON user_group_membership
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER user_group_membership_update_time
  BEFORE UPDATE
  ON user_group_membership
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();


create table group_permission_assignment
(
  id            serial primary key,
  group_id      int references user_group (id)       not null,
  permission_id int references group_permission (id) not null,
  create_time   timestamp                            NOT NULL DEFAULT current_timestamp,
  update_time   timestamp                            NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER group_permission_assignment_create_time
  BEFORE INSERT
  ON group_permission_assignment
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER group_permission_assignment_update_time
  BEFORE UPDATE
  ON group_permission_assignment
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

create unique index on group_permission_assignment (group_id, permission_id);

-- Audit

create table audit_event_type
(
  id              serial primary key,
  description_key varchar(255) not null
);

create table audit_log
(
  id               serial primary key,
  account_id       int          not null,
  event_type_id    int          not null references audit_event_type (id),
  description_args varchar(255),
  ip               varchar(255) not null,
  user_agent       varchar(255) not null,
  create_time      timestamp    NOT NULL DEFAULT current_timestamp,
  update_time      timestamp    NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER audit_log_create_time
  BEFORE INSERT
  ON audit_log
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER audit_log_update_time
  BEFORE UPDATE
  ON audit_log
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

-- Name history

create table name_record
(
  id            serial primary key,
  account_id    int          not null references account (id),
  previous_name varchar(255) not null,
  new_name      varchar(255) not null,
  create_time   timestamp    NOT NULL DEFAULT current_timestamp,
  update_time   timestamp    NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER name_record_create_time
  BEFORE INSERT
  ON name_record
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER name_record_update_time
  BEFORE UPDATE
  ON name_record
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

-- Map

create table map
(
  id           serial primary key,
  display_name varchar(100) not null,
  map_type     varchar(15)  not null,
  battle_type  varchar(15)  not null,
  uploader_id  int references account (id),
  create_time  timestamp    NOT NULL DEFAULT current_timestamp,
  update_time  timestamp    NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER map_create_time
  BEFORE INSERT
  ON map
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER map_update_time
  BEFORE UPDATE
  ON map
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

create unique index on map (lower(display_name));

create table map_version
(
  id          serial primary key,
  description text,
  max_players smallint     not null,
  width       smallint     not null,
  height      smallint     not null,
  version     smallint     not null,
  filename    varchar(200) not null,
  ranked      boolean               default true not null,
  hidden      boolean               default false not null,
  map_id      int          not null references map (id),
  create_time timestamp    NOT NULL DEFAULT current_timestamp,
  update_time timestamp    NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER map_version_create_time
  BEFORE INSERT
  ON map_version
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER map_version_update_time
  BEFORE UPDATE
  ON map_version
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

create unique index on map_version (lower(filename));
create unique index on map_version (map_id, version);

create table ladder_map
(
  id              serial primary key,
  map_version_id  int references map_version,
  featured_mod_id int references featured_mod (id),
  create_time     timestamp NOT NULL DEFAULT current_timestamp,
  update_time     timestamp NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER ladder_map_create_time
  BEFORE INSERT
  ON ladder_map
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER ladder_map_update_time
  BEFORE UPDATE
  ON ladder_map
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

create table map_version_review
(
  id             serial primary key,
  map_version_id int references map_version (id) not null,
  reviewer_id    int references account (id)     not null,
  score          smallint                        not null,
  text           text                            null,
  create_time    timestamp                       NOT NULL DEFAULT current_timestamp,
  update_time    timestamp                       NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER map_version_review_create_time
  BEFORE INSERT
  ON map_version_review
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER map_version_review_update_time
  BEFORE UPDATE
  ON map_version_review
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

create unique index on map_version_review (map_version_id, reviewer_id);

create FUNCTION update_map_version_review_summary()
  RETURNS TRIGGER AS
$body$
DECLARE
  new_positive          int;
  new_negative          int;
  new_score             int;
  new_reviews           int;
  new_lower_bound       float;
  positive_negative_sum int;
  p_map_version_id      int;
BEGIN
  IF TG_OP = 'INSERT' OR tg_op = 'UPDATE'
  THEN
    p_map_version_id = NEW.map_version_id;
  ELSIF TG_OP = 'DELETE'
  THEN
    p_map_version_id = OLD.map_version_id;
  END IF;

  SELECT COALESCE(sum((score - 1) * 0.25), 0),
         COALESCE(sum(1 - (score - 1) * 0.25), 0),
         COALESCE(sum(score), 0),
         COALESCE(count(*), 0)
         INTO new_positive, new_negative, new_score, new_reviews
  FROM map_version_review
  WHERE map_version_id = p_map_version_id;

  -- See https://onextrapixel.com/how-to-build-a-5-star-rating-system-with-wilson-interval-in-mysql/
  -- Prevent division by 0
  positive_negative_sum = f_if(new_positive + new_negative != 0, new_positive + new_negative, 1);
  new_lower_bound = COALESCE(
        ((new_positive + 1.9208) / positive_negative_sum -
         1.96 * SQRT((new_positive * new_negative) / positive_negative_sum + 0.9604) / positive_negative_sum) /
        (1 + 3.8416 / positive_negative_sum), 0);

  INSERT INTO map_version_review_summary (map_version_id, positive, negative, score, reviews, lower_bound)
  VALUES (p_map_version_id, new_positive, new_negative, new_score, new_reviews, new_lower_bound)
  ON CONFLICT (map_version_id) DO UPDATE SET
    positive = new_positive,
    negative = new_negative,
    score = new_score,
    reviews = new_reviews,
    lower_bound = new_lower_bound;
  RETURN NULL;
END;
$body$
  language plpgsql;

create trigger update_map_version_review_summary_after_insert_update
  after insert or update or delete
  on map_version_review
  for each row
execute procedure update_map_version_review_summary();

create table map_version_review_summary
(
  id             serial primary key,
  map_version_id int       not null references map_version (id) unique,
  positive       float     not null,
  negative       float     not null,
  score          float     not null,
  reviews        int       not null,
  lower_bound    float     not null,
  create_time    timestamp NOT NULL DEFAULT current_timestamp,
  update_time    timestamp NOT NULL DEFAULT current_timestamp
);

CREATE TRIGGER map_version_review_summary_create_time
  BEFORE INSERT
  ON map_version_review_summary
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER map_version_review_summary_update_time
  BEFORE UPDATE
  ON map_version_review_summary
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

CREATE VIEW map_review_summary AS
SELECT
  -- Keep an `id` field to easily allow us to switch to a table later
  map_id                                AS id,
  map_id                                AS map_id,
  sum(positive)                         AS positive,
  sum(negative)                         AS negative,
  sum(score)                            AS score,
  sum(reviews)                          AS reviews,
  sum(summary.weighted_bound) / f_if(sum(summary.reviews) != 0, cast(sum(summary.reviews) as int),
                                     1) AS lower_bound
FROM (SELECT *, reviews * lower_bound AS weighted_bound FROM map_version_review_summary) summary
       JOIN map_version ON map_version.id = summary.map_version_id
GROUP BY map_id;

create view map_review_scores as
select CONCAT(map_id, '_', score) as id, map_id, score, count(*)
from map_version_review
       join map_version mv on map_version_review.map_version_id = mv.id
       join map m on mv.map_id = m.id
group by map_id, score;

create view map_version_review_scores as
select CONCAT(map_version_id, '_', score) as id, map_version_id, score, count(*)
from map_version_review
group by map_version_id, score;

-- Leaderboard

create table leaderboard
(
  id              serial primary key,
  technical_name  varchar(255) not null,
  name_key        varchar(255) not null,
  description_key varchar(255) not null,
  create_time     timestamp    NOT NULL DEFAULT current_timestamp,
  update_time     timestamp    NOT NULL DEFAULT current_timestamp
);
comment on column leaderboard.name_key is 'A stable, short name that allows applications to hardcode certain leaderboards (e.g. ''ladder1v1'', ''global''). Preferably, applications should implement leaderboards dynamically.';
comment on column leaderboard.name_key is 'The leaderboard''s i18n name key';
comment on column leaderboard.name_key is 'The leaderboard''s i18n description key';
comment on table leaderboard is 'Whenever a player plays a ranked game, its outcome will affect the rating of a specific leadboard (e.g. ''global'' or ''ladder1v1''). The existing leaderboards are defined in this table.';

CREATE TRIGGER leaderboard_create_time
  BEFORE INSERT
  ON leaderboard
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER leaderboard_update_time
  BEFORE UPDATE
  ON leaderboard
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

-- Matchmaker pool

create table matchmaker_pool
(
  id              serial primary key,
  featured_mod_id int          not null references featured_mod,
  leaderboard_id  int          not null references leaderboard,
  name_key        varchar(255) not null,
  create_time     timestamp    NOT NULL DEFAULT current_timestamp,
  update_time     timestamp    NOT NULL DEFAULT current_timestamp
);
comment on table matchmaker_pool is 'When using the matchmaker, players will be added to a specific matchmaker pool (e.g. ''ladder1v1'', ''ladder2v2''). Players within the same pool can be matched. The pool specifies which featured mod will be played and which leaderboard will be used to look up and update a player''s rating.';

CREATE TRIGGER matchmaker_pool_create_time
  BEFORE INSERT
  ON matchmaker_pool
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER matchmaker_pool_update_time
  BEFORE UPDATE
  ON matchmaker_pool
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

-- Game

create type game_validity as enum (
  'VALID',
  'TOO_MANY_DESYNCS',
  'WRONG_VICTORY_CONDITION',
  'NO_FOG_OF_WAR',
  'CHEATS_ENABLED',
  'PREBUILT_ENABLED',
  'NORUSH_ENABLED',
  'BAD_UNIT_RESTRICTIONS',
  'BAD_MAP',
  'TOO_SHORT',
  'BAD_MOD',
  'COOP_NOT_RANKED',
  'MUTUAL_DRAW',
  'SINGLE_PLAYER',
  'FFA_NOT_RANKED',
  'UNEVEN_TEAMS_NOT_RANKED',
  'UNKNOWN_RESULT',
  'TEAMS_UNLOCKED',
  'MULTIPLE_TEAMS',
  'HAS_AI',
  'STALE',
  'SERVER_SHUTDOWN'
  );

create type victory_condition as enum ('DEMORALIZATION', 'DOMINATION', 'ERADICATION', 'SANDBOX');

-- Creating an explicit sequence which will be used by the server code to get the next ID w/o saving a game
create sequence game_id_seq;
create table game
(
  id                int primary key            default nextval('game_id_seq'),
  start_time        timestamp         not null,
  end_time          timestamp,
  featured_mod_id   int               not null references featured_mod (id),
  host_id           int               not null references account (id),
  map_version_id    int               not null references map_version (id),
  name              varchar(255)      not null,
  validity          game_validity     not null,
  victory_condition victory_condition not null,
  create_time       timestamp         NOT NULL DEFAULT current_timestamp,
  update_time       timestamp         NOT NULL DEFAULT current_timestamp
);
comment on column game.id is 'The server needs to handle sequences by itself, thus we use the sequence rather than serial here.';
ALTER SEQUENCE game_id_seq
  OWNED BY game.id;

CREATE TRIGGER game_create_time
  BEFORE INSERT
  ON game
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER game_update_time
  BEFORE UPDATE
  ON game
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

create index game_featured_mod_index
  on game (featured_mod_id);

create index game_map_version_id_index
  on game (map_version_id);

create index game_validity_index
  on game (validity);

-- Game Review

create table game_review
(
  id          serial primary key,
  game_id     int       not null references game (id),
  reviewer_id int       not null references account (id),
  score       smallint  not null,
  text        text,
  create_time timestamp NOT NULL DEFAULT current_timestamp,
  update_time timestamp NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER game_review_create_time
  BEFORE INSERT
  ON game_review
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER game_review_update_time
  BEFORE UPDATE
  ON game_review
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

create unique index game_review_game_id_reviewer_id_uindex
  on game_review (game_id, reviewer_id);


create table game_review_summary
(
  id          serial primary key,
  game_id     int unique not null references game (id),
  positive    float      not null,
  negative    float      not null,
  score       float      not null,
  reviews     int        not null,
  lower_bound float,
  create_time timestamp  NOT NULL DEFAULT current_timestamp,
  update_time timestamp  NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER game_review_summary_create_time
  BEFORE INSERT
  ON game_review_summary
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER game_review_summary_update_time
  BEFORE UPDATE
  ON game_review_summary
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

create function update_game_review_summary()
  returns trigger as
$body$
DECLARE
  new_positive          int;
  new_negative          int;
  new_score             int;
  new_reviews           int;
  new_lower_bound       float;
  positive_negative_sum int;
  p_game_id             int;
BEGIN
  IF TG_OP = 'INSERT' OR tg_op = 'UPDATE'
  THEN
    p_game_id = NEW.game_id;
  ELSIF TG_OP = 'DELETE'
  THEN
    p_game_id = OLD.game_id;
  END IF;

  SELECT coalesce(sum((score - 1) * 0.25), 0),
         coalesce(sum(1 - (score - 1) * 0.25), 0),
         coalesce(sum(score), 0),
         coalesce(count(*), 0)
         INTO new_positive, new_negative, new_score, new_reviews
  FROM game_review
  WHERE game_id = p_game_id;

  -- See https://onextrapixel.com/how-to-build-a-5-star-rating-system-with-wilson-interval-in-mysql/
  -- Prevent division by 0
  positive_negative_sum = f_if(new_positive + new_negative != 0, new_positive + new_negative, 1);
  new_lower_bound = coalesce(
        ((new_positive + 1.9208) / positive_negative_sum -
         1.96 * SQRT((new_positive * new_negative) / positive_negative_sum + 0.9604) / positive_negative_sum) /
        (1 + 3.8416 / positive_negative_sum), 0);

  INSERT INTO game_review_summary (game_id, positive, negative, score, reviews, lower_bound)
  VALUES (p_game_id, new_positive, new_negative, new_score, new_reviews, new_lower_bound)
  ON CONFLICT (game_id) DO UPDATE SET
    positive = new_positive,
    negative = new_negative,
    score = new_score,
    reviews = new_reviews,
    lower_bound = new_lower_bound;
  RETURN NULL;
END;
$body$
  language plpgsql;

create trigger update_game_review_summary_after_insert_update
  after insert or update or delete
  on game_review
  for each row
execute procedure update_game_review_summary();

create view game_review_scores as
select CONCAT(game_id, '_', score) as id, game_id, score, count(*)
from game_review
group by game_id, score;

-- Game participation

create type game_participant_outcome as enum ('VICTORY', 'DRAW', 'DEFEAT');
create table game_participant
(
  id             serial primary key,
  game_id        int       not null references game (id),
  participant_id int       not null references account (id),
  faction        integer,
  color          smallint  not null,
  team           smallint  not null,
  start_spot     smallint  not null,
  score          smallint  not null,
  outcome        game_participant_outcome,
  finish_time    timestamp,
  create_time    timestamp NOT NULL DEFAULT current_timestamp,
  update_time    timestamp NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER game_participant_create_time
  BEFORE INSERT
  ON game_participant
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER game_participant_update_time
  BEFORE UPDATE
  ON game_participant
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

create index game_participant_game_id_index
  on game_participant (game_id);

create index game_participant_finish_time_index
  on game_participant (finish_time);

create index game_participant_account_id_index
  on game_participant (participant_id);


-- Leaderboard rating

create table leaderboard_rating_journal
(
  id                      serial primary key,
  game_participant_id     int references game_participant (id),
  leaderboard_id          int references leaderboard (id),
  rating_mean_before      real      not null,
  rating_deviation_before real      not null,
  rating_mean_after       real      not null,
  rating_deviation_after  real      not null,
  create_time             timestamp NOT NULL DEFAULT current_timestamp,
  update_time             timestamp NOT NULL DEFAULT current_timestamp
);

CREATE TRIGGER leaderboard_rating_journal_create_time
  BEFORE INSERT
  ON leaderboard_rating_journal
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER leaderboard_rating_journal_update_time
  BEFORE UPDATE
  ON leaderboard_rating_journal
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();


-- Coop

create type coop_mission_type as enum ('FA', 'AEON', 'CYBRAN', 'UEF', 'CUSTOM');
CREATE TABLE coop_mission
(
  id             serial PRIMARY KEY,
  map_version_id int references map_version (id) not null,
  type           coop_mission_type               NOT NULL,
  name           varchar(40)                     NOT NULL,
  description    text,
  create_time    timestamp                       NOT NULL DEFAULT current_timestamp,
  update_time    timestamp                       NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER coop_mission_create_time
  BEFORE INSERT
  ON coop_mission
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER coop_mission_update_time
  BEFORE UPDATE
  ON coop_mission
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

CREATE UNIQUE INDEX ON coop_mission (lower(name));

CREATE TABLE coop_leaderboard
(
  id                serial PRIMARY KEY,
  coop_mission_id   int       NOT NULL references coop_mission (id),
  game_id           int       NOT NULL references game (id),
  secondary_targets boolean   NOT NULL,
  game_time         integer   NOT NULL,
  player_count      int       NOT NULL,
  create_time       timestamp NOT NULL DEFAULT current_timestamp,
  update_time       timestamp NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER coop_leaderboard_create_time
  BEFORE INSERT
  ON coop_leaderboard
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER coop_leaderboard_update_time
  BEFORE UPDATE
  ON coop_leaderboard
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

COMMENT ON COLUMN coop_leaderboard.secondary_targets
  IS 'Whether or not secondary targets have been completed';
COMMENT ON COLUMN coop_leaderboard.game_time
  IS 'Time it took to complete the mission, in seconds of simulation time';

-- Message
create table message
(
  id          serial primary key,
  key         varchar(255) not null,
  language    char(2)      not null,
  region      char(2)      null,
  value       text         null,
  create_time timestamp    NOT NULL DEFAULT current_timestamp,
  update_time timestamp    NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER message_create_time
  BEFORE INSERT
  ON message
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER message_update_time
  BEFORE UPDATE
  ON message
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

create unique index on message (lower(key), lower(language), lower(region));

-- Mod

create table mod
(
  id           serial primary key,
  display_name varchar(100) not null,
  author       varchar(100) not null,
  uploader_id  int          not null references account (id),
  create_time  timestamp    NOT NULL DEFAULT current_timestamp,
  update_time  timestamp    NOT NULL DEFAULT current_timestamp
);

CREATE TRIGGER mod_create_time
  BEFORE INSERT
  ON mod
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER mod_update_time
  BEFORE UPDATE
  ON mod
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

create unique index on mod (lower(display_name));

CREATE TYPE mod_type AS ENUM ('UI', 'SIM');
create table mod_version
(
  id          serial primary key,
  uuid        uuid unique             not null,
  type        mod_type                not null,
  description text                    not null,
  version     smallint                not null,
  filename    varchar(255) unique     not null,
  icon        varchar(255)            null,
  ranked      boolean                          default false not null,
  hidden      boolean                          default false not null,
  mod_id      int references mod (id) not null,
  create_time timestamp               NOT NULL DEFAULT current_timestamp,
  update_time timestamp               NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER mod_version_create_time
  BEFORE INSERT
  ON mod_version
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER mod_version_update_time
  BEFORE UPDATE
  ON mod_version
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

create unique index on mod_version (lower(filename));
create unique index on mod_version (mod_id, version);

create table mod_version_review
(
  id             serial primary key,
  mod_version_id int references mod_version (id) not null,
  reviewer_id    int references account (id)     not null,
  score          smallint                        not null,
  text           text                            null,
  create_time    timestamp                       NOT NULL DEFAULT current_timestamp,
  update_time    timestamp                       NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER mod_version_review_create_time
  BEFORE INSERT
  ON mod_version
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER mod_version_review_update_time
  BEFORE UPDATE
  ON mod_version_review
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

create unique index on mod_version_review (mod_version_id, reviewer_id);

create function update_mod_version_review_summary()
  returns trigger as
$body$
DECLARE
  new_positive          int;
  new_negative          int;
  new_score             int;
  new_reviews           int;
  new_lower_bound       float;
  positive_negative_sum int;
  p_mod_version_id      int;
BEGIN
  IF TG_OP = 'INSERT' OR tg_op = 'UPDATE'
  THEN
    p_mod_version_id = NEW.mod_version_id;
  ELSIF TG_OP = 'DELETE'
  THEN
    p_mod_version_id = OLD.mod_version_id;
  END IF;

  SELECT coalesce(sum((score - 1) * 0.25), 0),
         coalesce(sum(1 - (score - 1) * 0.25), 0),
         coalesce(sum(score), 0),
         coalesce(count(*), 0)
         INTO new_positive, new_negative, new_score, new_reviews
  FROM mod_version_review
  WHERE mod_version_id = p_mod_version_id;

  -- See https://onextrapixel.com/how-to-build-a-5-star-rating-system-with-wilson-interval-in-mysql/
  -- Prevent division by 0
  positive_negative_sum = f_if(new_positive + new_negative != 0, new_positive + new_negative, 1);
  new_lower_bound = coalesce(
        ((new_positive + 1.9208) / positive_negative_sum -
         1.96 * SQRT((new_positive * new_negative) / positive_negative_sum + 0.9604) / positive_negative_sum) /
        (1 + 3.8416 / positive_negative_sum), 0);

  INSERT INTO mod_version_review_summary (mod_version_id, positive, negative, score, reviews, lower_bound)
  VALUES (p_mod_version_id, new_positive, new_negative, new_score, new_reviews, new_lower_bound)
  ON CONFLICT (mod_version_id) DO UPDATE SET
    positive = new_positive,
    negative = new_negative,
    score = new_score,
    reviews = new_reviews,
    lower_bound = new_lower_bound;
  RETURN NULL;
END
$body$
  language plpgsql;


create trigger update_mod_version_review_summary_after_insert_update
  after insert or update or delete
  on mod_version_review
  for each row
execute procedure update_mod_version_review_summary();

create table mod_version_review_summary
(
  id             serial primary key,
  mod_version_id int references mod_version (id) unique not null,
  positive       float                                  not null,
  negative       float                                  not null,
  score          float                                  not null,
  reviews        int                                    not null,
  lower_bound    float                                  not null,
  create_time    timestamp                              NOT NULL DEFAULT current_timestamp,
  update_time    timestamp                              NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER mod_version_review_summary_create_time
  BEFORE INSERT
  ON mod_version_review_summary
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER mod_version_review_summary_update_time
  BEFORE UPDATE
  ON mod_version_review_summary
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

CREATE VIEW mod_review_summary AS
SELECT
  -- Keep an `id` field to easily allow us to switch to a table later
  mod_id                                AS id,
  mod_id                                AS mod_id,
  sum(positive)                         AS positive,
  sum(negative)                         AS negative,
  sum(score)                            AS score,
  sum(reviews)                          AS reviews,
  sum(summary.weighted_bound) / f_if(sum(summary.reviews) != 0, cast(sum(summary.reviews) as int),
                                     1) AS lower_bound
FROM (SELECT *, reviews * lower_bound AS weighted_bound FROM mod_version_review_summary) summary
       JOIN mod_version ON mod_version.id = summary.mod_version_id
GROUP BY mod_id;

create view mod_review_scores as
select CONCAT(mod_id, '_', score) as id, mod_id, score, count(*)
from mod_version_review
       join mod_version mv on mod_version_review.mod_version_id = mv.id
       join mod m on mv.mod_id = m.id
group by mod_id, score;

create view mod_version_review_scores as
select CONCAT(mod_version_id, '_', score) as id, mod_version_id, score, count(*)
from mod_version_review
group by mod_version_id, score;

-- OAuth2

create table oauth_client
(
  id                   serial primary key,
  name                 varchar(255) unique not null,
  client_id            uuid unique         not null default uuid_generate_v4(),
  client_secret        varchar(255) unique not null,
  redirect_uris        text                not null,
  default_redirect_uri varchar(2000)       not null,
  default_scope        text                not null,
  icon_url             varchar(2000)       null,
  auto_approve_scopes  text                null,
  create_time          timestamp           NOT NULL DEFAULT current_timestamp,
  update_time          timestamp           NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER oauth_client_create_time
  BEFORE INSERT
  ON oauth_client
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER oauth_client_update_time
  BEFORE UPDATE
  ON oauth_client
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();
create unique index on oauth_client (lower(name));

create table player_achievement
(
  id             serial primary key,
  player_id      int references account (id)                 not null,
  achievement_id uuid references achievement_definition (id) not null,
  current_steps  int,
  state          achievement_state                           not null,
  create_time    timestamp                                   NOT NULL DEFAULT current_timestamp,
  update_time    timestamp                                   NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER player_achievement_create_time
  BEFORE INSERT
  ON player_achievement
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER player_achievement_update_time
  BEFORE UPDATE
  ON player_achievement
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

create unique index on player_achievement (player_id, achievement_id);


create view achievement_statistics as
select ach.id                          AS achievement_id,
       coalesce(unlock_stats.COUNT, 0) AS unlockers_count,
       coalesce(
           round((100 * (unlock_stats.COUNT / f_if(achievers_count.COUNT != 0, cast(achievers_count.COUNT as int), 1))),
                 2),
           0)                          AS unlockers_percent,
       unlock_stats.min_time           AS unlockers_min_duration,
       unlock_stats.avg_time           AS unlockers_avg_duration,
       unlock_stats.max_time           AS unlockers_max_duration
from achievement_definition ach
       left join (select pa.achievement_id AS achievement_id,
                         count(0)          AS COUNT,
                         round(min(EXTRACT(EPOCH FROM pa.update_time -
                                                      greatest(ach.create_time,
                                                               account.create_time)) /
                                   3600))  AS min_time,
                         round(avg(EXTRACT(EPOCH FROM pa.update_time -
                                                      greatest(ach.create_time,
                                                               account.create_time)) /
                                   3600))  AS avg_time,
                         round(max(EXTRACT(EPOCH FROM pa.update_time -
                                                      greatest(ach.create_time,
                                                               account.create_time)) /
                                   3600))  AS max_time
                  from achievement_definition ach
                         left join player_achievement pa on pa.achievement_id = ach.id
                         left join account on account.id = pa.player_id
                  where pa.state = 'UNLOCKED'
                  group by pa.achievement_id) unlock_stats on unlock_stats.achievement_id = ach.id,
     (select count(0) AS COUNT
      from account
      where account.id in (select player_achievement.player_id from player_achievement)) achievers_count;

create table player_event
(
  id          serial primary key,
  player_id   int references account (id)           not null,
  event_id    uuid references event_definition (id) not null,
  count       int                                   not null,
  create_time timestamp                             NOT NULL DEFAULT current_timestamp,
  update_time timestamp                             NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER player_event_create_time
  BEFORE INSERT
  ON player_event
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER player_event_update_time
  BEFORE UPDATE
  ON player_event
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

create unique index on player_event (player_id, event_id);

-- Moderation report

CREATE TYPE moderation_report_status AS ENUM ('AWAITING', 'PROCESSING', 'COMPLETED', 'DISCARDED');
create table moderation_report
(
  id                     serial primary key,
  reporter_id            int references account (id) not null,
  report_description     text,
  game_id                int references game (id),
  report_status          moderation_report_status    not null,
  game_incident_timecode varchar(100),
  public_note            text,
  private_note           text,
  last_moderator_id      int references account (id),
  create_time            timestamp                   NOT NULL DEFAULT current_timestamp,
  update_time            timestamp                   NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER moderation_report_create_time
  BEFORE INSERT
  ON moderation_report
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER moderation_report_update_time
  BEFORE UPDATE
  ON moderation_report
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();
COMMENT ON COLUMN moderation_report.game_id
  IS 'If NULL, the offense did not happen in game.';
COMMENT ON COLUMN moderation_report.report_status
  IS 'The current status of the report. This will be accessable by the concerned user.';
COMMENT ON COLUMN moderation_report.game_incident_timecode
  IS 'The timecode of the incident ingame, indicated by the user in their own terms.';
COMMENT ON COLUMN moderation_report.public_note
  IS 'A public notice left by the moderator which will be addressed to the reporter once the report is marked as either COMPLETED or DISCARDED.';
COMMENT ON COLUMN moderation_report.private_note
  IS 'A private notice left by the moderator which any other moderator accessing the record will be able to see.';
COMMENT ON COLUMN moderation_report.last_moderator_id
  IS 'Last moderator to have updated the report.';

create index on moderation_report (last_moderator_id);

create table reported_user
(
  id          serial primary key,
  account_id  int references account (id)           not null,
  report_id   int references moderation_report (id) not null,
  create_time timestamp                             NOT NULL DEFAULT current_timestamp,
  update_time timestamp                             NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER reported_user_create_time
  BEFORE INSERT
  ON reported_user
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER reported_user_update_time
  BEFORE UPDATE
  ON reported_user
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

-- Teamkill

create table teamkill_report
(
  id            serial primary key,
  teamkiller_id int       not null references account (id),
  victim_id     int       not null references account (id),
  game_id       int       not null references game (id),
  game_time     int       not null,
  create_time   timestamp NOT NULL DEFAULT current_timestamp,
  update_time   timestamp NOT NULL DEFAULT current_timestamp
);
COMMENT ON COLUMN teamkill_report.game_time
  IS 'How many seconds into the game, in simulation time';
CREATE TRIGGER teamkill_report_create_time
  BEFORE INSERT
  ON teamkill_report
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER teamkill_report_update_time
  BEFORE UPDATE
  ON teamkill_report
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

comment on column teamkill_report.teamkiller_id
  is 'The player who performed the teamkill.';
comment on column teamkill_report.victim_id
  is 'The player who got teamkilled.';
comment on column teamkill_report.game_id
  is 'The game in which the teamkill was performed.';
comment on column teamkill_report.game_time
  is 'Time of game in seconds when the teamkill was performed.';

create unique index on teamkill_report (teamkiller_id, victim_id, game_id);

-- Ban

CREATE TYPE ban_scope AS ENUM ('CHAT', 'MATCHMAKER', 'GLOBAL');

create table ban
(
  id                   serial primary key,
  account_id           int          not null references account (id),
  author_id            int          not null references account (id),
  report_id            int references moderation_report (id),
  reason               varchar(255) not null,
  expiry_time          timestamp,
  scope                varchar(255) not null,
  revocation_time      timestamp,
  revocation_reason    varchar(255),
  revocation_author_id int references account (id),
  create_time          timestamp    NOT NULL DEFAULT current_timestamp,
  update_time          timestamp    NOT NULL DEFAULT current_timestamp
);

CREATE TRIGGER ban_create_time
  BEFORE INSERT
  ON ban
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER ban_update_time
  BEFORE UPDATE
  ON ban
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

-- Rating

create table leaderboard_rating
(
  id             serial primary key,
  account_id     int       not null references account (id),
  mean           float     not null,
  deviation      float     not null,
  rating         float     not null,
  total_games    int       not null,
  won_games      int       not null,
  leaderboard_id int       not null references leaderboard,
  create_time    timestamp NOT NULL DEFAULT current_timestamp,
  update_time    timestamp NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER rating_create_time
  BEFORE INSERT
  ON leaderboard_rating
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER rating_update_time
  BEFORE UPDATE
  ON leaderboard_rating
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

create or replace function calculate_rating()
  returns trigger AS
$$
begin
  NEW.rating = NEW.mean - 3 * NEW.deviation;
  return NEW;
end;
$$
  language plpgsql;
COMMENT ON COLUMN leaderboard_rating.rating
  IS 'Calculated by database trigger';

CREATE TRIGGER rating_calculate_rating
  BEFORE UPDATE OR INSERT
  ON leaderboard_rating
  FOR EACH ROW
EXECUTE PROCEDURE calculate_rating();

create materialized view leaderboard_entry as
SELECT leaderboard_rating.id,
       account.id                                 as player_id,
       account.display_name                       as player_name,
       leaderboard_rating.rating                  as rating,
       leaderboard_rating.total_games             as total_games,
       leaderboard_rating.won_games               as won_games,
       leaderboard_rating.leaderboard_id          as leaderboard_id,
       rank() over (PARTITION BY leaderboard_rating.leaderboard_id
         ORDER BY leaderboard_rating.rating desc) as position,
       leaderboard_rating.create_time             as create_time,
       now()                                      as update_time
FROM leaderboard_rating
       JOIN account on account.id = leaderboard_rating.account_id
WHERE leaderboard_rating.update_time > now() - interval '30' day
  AND account.id NOT IN (SELECT account_id
                         FROM ban
                         WHERE expiry_time is null
                            or expiry_time > NOW())
ORDER BY position asc;

-- Tutorial

create table tutorial_category
(
  id              serial primary key,
  technical_name  varchar(255) not null,
  category_key    varchar(255) not null,
  description_key varchar(255) null,
  create_time     timestamp    NOT NULL DEFAULT current_timestamp,
  update_time     timestamp    NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER tutorial_create_time
  BEFORE INSERT
  ON tutorial_category
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER tutorial_category_update_time
  BEFORE UPDATE
  ON tutorial_category
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

create table tutorial
(
  id              serial primary key,
  map_version_id  int references map_version (id),
  title_key       varchar(255)                          not null,
  description_key varchar(255),
  category        int references tutorial_category (id) not null,
  image           varchar(255),
  ordinal         int                                   not null,
  launchable      boolean                               not null,
  technical_name  varchar(255)                          not null,
  create_time     timestamp                             NOT NULL DEFAULT current_timestamp,
  update_time     timestamp                             NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER tutorial_create_time
  BEFORE INSERT
  ON tutorial
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER tutorial_update_time
  BEFORE UPDATE
  ON tutorial
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

comment on column tutorial.title_key
  is 'The key for the title of the tutorial.';
comment on column tutorial.description_key
  is 'The key for the description of the tutorial. The text in the messages table must be in HTML.';
comment on column tutorial.image
  is 'The ''image''-path for to a preview image of the tutorial, relative to the base url defined in the api';
comment on column tutorial.ordinal
  is 'The ''ordinal'' which defines in which order the tutorials are shown in the client (within their category)';
comment on column tutorial.launchable
  is 'Boolean that defines whether or not the tutorial can be launched';
comment on column tutorial.technical_name
  is 'The ''technical_name'' of the tutorial given to the tutorial mod to decide what tutorial to start, when implemented';


create table tutorial_section
(
  id          serial primary key,
  section     varchar(45)  not null,
  description varchar(100) not null,
  create_time timestamp    NOT NULL DEFAULT current_timestamp,
  update_time timestamp    NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER tutorial_section_create_time
  BEFORE INSERT
  ON tutorial_section
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER tutorial_sections_update_time
  BEFORE UPDATE
  ON tutorial_section
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

-- Account notes

create table account_note
(
  id          serial primary key,
  account_id  int references account (id) not null references account (id),
  author_id   int references account (id) not null,
  watched     boolean                              default false not null,
  note        text                        not null,
  create_time timestamp                   NOT NULL DEFAULT current_timestamp,
  update_time timestamp                   NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER account_note_create_time
  BEFORE INSERT
  ON account_note
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER account_note_update_time
  BEFORE UPDATE
  ON account_note
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

comment on column account_note.watched
  is 'Marks notes that should be reviewed at a later time (i.e. gather facts before applying a ban)';

create index on account_note (watched);

-- Voting

create table voting_subject
(
  id                 serial primary key,
  subject_key        varchar(255) not null,
  begin_of_vote_time timestamp             default current_timestamp not null,
  end_of_vote_time   timestamp    not null,
  min_games_to_vote  smallint              default 0 not null,
  description_key    varchar(255) null,
  topic_url          text         null,
  reveal_winner      boolean      not null default false,
  create_time        timestamp    NOT NULL DEFAULT current_timestamp,
  update_time        timestamp    NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER voting_subject_create_time
  BEFORE INSERT
  ON voting_subject
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER voting_subject_update_time
  BEFORE UPDATE
  ON voting_subject
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();
comment on column voting_subject.subject_key
  is 'With this key the ''subject'' can be loaded from the messages table';
comment on column voting_subject.min_games_to_vote
  is 'The number of games a player must have to in order to vote';
comment on column voting_subject.description_key
  is 'The ''description-key'' of the voting subject with it additional information (HTML) can be loaded from the messages table';
comment on column voting_subject.topic_url
  is 'An URL to a page with additional information, e.g. a forum post.';
comment on column voting_subject.reveal_winner
  is 'When set to true after voting ended the winner is calculated and shown publicly';

create table vote
(
  id                serial primary key,
  voter_id          int       not null references account (id),
  voting_subject_id int       not null references voting_subject (id),
  create_time       timestamp NOT NULL DEFAULT current_timestamp,
  update_time       timestamp NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER vote_create_time
  BEFORE INSERT
  ON vote
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER vote_update_time
  BEFORE UPDATE
  ON vote
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();
create unique index on vote (voter_id, voting_subject_id);

create table voting_question
(
  id                 serial primary key,
  max_answers        int     default 1                  not null,
  question_key       varchar(255)                       not null,
  voting_subject_id  int references voting_subject (id) not null,
  description_key    varchar(255),
  ordinal            int     default 0                  not null,
  alternative_voting boolean default false              not null,
  create_time        timestamp                          NOT NULL DEFAULT current_timestamp,
  update_time        timestamp                          NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER voting_question_create_time
  BEFORE INSERT
  ON voting_question
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER voting_question_update_time
  BEFORE UPDATE
  ON voting_question
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();
comment on column voting_question.question_key
  is 'With this key the ''question'' can be loaded from the messages table';
comment on column voting_question.description_key
  is 'The ''description-key'' of the voting question, with it additional information (HTML) can be loaded from the messages table';
comment on column voting_question.ordinal
  is 'The ''ordinal'' of the voting question, in the client questions are shown according to their ordinal value';
comment on column voting_question.alternative_voting
  is 'Defines whether alternative voting applies to this question';


create table voting_choice
(
  id                 serial primary key,
  choice_text_key    varchar(255)                        not null,
  voting_question_id int references voting_question (id) not null,
  description_key    varchar(255)                        not null,
  ordinal            int                                          default 0 not null,
  create_time        timestamp                           NOT NULL DEFAULT current_timestamp,
  update_time        timestamp                           NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER voting_choice_create_time
  BEFORE INSERT
  ON voting_choice
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER voting_choice_update_time
  BEFORE UPDATE
  ON voting_choice
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

comment on column voting_choice.choice_text_key
  is 'With this key the ''choice_text'' can be loaded from the messages table.';
comment on column voting_choice.description_key
  is 'The ''description- key '' of the voting choice, with it additional information (HTML) can be loaded from the messages table.';
comment on column voting_choice.ordinal
  is 'The ''ordinal'' of the voting choice, in the client choices are shown according to their ordinal value.';

create table voting_answer
(
  id                  serial primary key,
  vote_id             int references vote (id)          not null,
  voting_choice_id    int references voting_choice (id) not null,
  alternative_ordinal smallint                          null,
  create_time         timestamp                         NOT NULL DEFAULT current_timestamp,
  update_time         timestamp                         NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER voting_answer_create_time
  BEFORE INSERT
  ON voting_answer
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER voting_answer_update_time
  BEFORE UPDATE
  ON voting_answer
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

comment on column voting_answer.alternative_ordinal
  is 'Defines what preference for alternative voting this answer is';

create unique index on voting_answer (vote_id, voting_choice_id);


create table voting_question_winner
(
  id                 serial primary key,
  voting_question_id int references voting_question (id) not null,
  voting_choice_id   int references voting_choice (id)   not null,
  create_time        timestamp                           NOT NULL DEFAULT current_timestamp,
  update_time        timestamp                           NOT NULL DEFAULT current_timestamp
);
CREATE TRIGGER voting_question_winner_create_time
  BEFORE INSERT
  ON voting_question_winner
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER voting_question_winner_update_time
  BEFORE UPDATE
  ON voting_question_winner
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

create unique index on voting_question_winner (voting_question_id, voting_choice_id);

-- Map statistics

create view map_statistic as
select map_id as id, count(*) as times_played, now() as create_time, now() as update_time
from game
       join map_version mv on game.map_version_id = mv.id
       join map m on mv.map_id = m.id
group by mv.map_id;

-- Map version statistics

create view map_version_statistic as
select mv.id as id, count(*) as times_played, now() as create_time, now() as update_time
from game
       join map_version mv on game.map_version_id = mv.id
group by mv.id;

-- Division

create table ladder_division
(
  id          serial primary key,
  name        varchar(255) not null unique,
  league      smallint     not null,
  threshold   int          not null,
  create_time timestamp    NOT NULL DEFAULT current_timestamp,
  update_time timestamp    NOT NULL DEFAULT current_timestamp
);

CREATE TRIGGER ladder_division_create_time
  BEFORE INSERT
  ON ladder_division
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER ladder_division_update_time
  BEFORE UPDATE
  ON ladder_division
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();


create table ladder_division_score
(
  id          serial primary key,
  season      smallint  not null,
  account_id  int       not null references account (id),
  league      smallint  not null,
  score       float     not null,
  games       smallint  null,
  create_time timestamp NOT NULL DEFAULT current_timestamp,
  update_time timestamp NOT NULL DEFAULT current_timestamp
);

create unique index on ladder_division_score (season, account_id);
create index league
  on ladder_division_score (league);
create index user_id
  on ladder_division_score (account_id);

CREATE TRIGGER ladder_division_score_create_time
  BEFORE INSERT
  ON ladder_division_score
  FOR EACH ROW
EXECUTE PROCEDURE set_insert_times();
CREATE TRIGGER ladder_division_score_update_time
  BEFORE UPDATE
  ON ladder_division_score
  FOR EACH ROW
EXECUTE PROCEDURE update_update_time();

