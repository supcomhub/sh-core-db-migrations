alter table matchmaker_pool
  add technical_name varchar(255) not null;

create unique index matchmaker_pool_technical_name_uindex
  on matchmaker_pool (technical_name);
