DELETE FROM user_group_membership;
DELETE FROM group_permission_assignment;
DELETE FROM user_group;
DELETE FROM group_permission;


SELECT setval(pg_get_serial_sequence('group_permission', 'id'), max(id))
FROM group_permission;


SELECT setval(pg_get_serial_sequence('user_group', 'id'), max(id))
FROM user_group;


SELECT setval(pg_get_serial_sequence('user_group_membership', 'id'), max(id))
FROM user_group_membership;


SELECT setval(pg_get_serial_sequence('group_permission_assignment', 'id'), max(id))
FROM group_permission_assignment;


INSERT INTO group_permission (id, technical_name, name_key) VALUES (1, 'WRITE_AVATAR', 'permission.write_avatar');
INSERT INTO group_permission (id, technical_name, name_key) VALUES (2, 'WRITE_MATCHMAKER_MAP', 'permission.write_matchmaker_map');
INSERT INTO group_permission (id, technical_name, name_key) VALUES (3, 'WRITE_EMAIL_DOMAIN_BAN', 'permission.write_email_domain_ban');
INSERT INTO group_permission (id, technical_name, name_key) VALUES (4, 'ADMIN_VOTE', 'permission.admin_vote');
INSERT INTO group_permission (id, technical_name, name_key) VALUES (5, 'WRITE_USER_GROUP', 'permission.write_user_group');
INSERT INTO group_permission (id, technical_name, name_key) VALUES (6, 'READ_USER_GROUP', 'permission.read_user_group');
INSERT INTO group_permission (id, technical_name, name_key) VALUES (7, 'WRITE_TUTORIAL', 'permission.write_tutorial');
INSERT INTO group_permission (id, technical_name, name_key) VALUES (8, 'WRITE_NEWS_POST', 'permission.write_news_post');
INSERT INTO group_permission (id, technical_name, name_key) VALUES (9, 'WRITE_OAUTH_CLIENT', 'permission.write_oauth_client');
INSERT INTO group_permission (id, technical_name, name_key) VALUES (10, 'ADMIN_MAP', 'permission.admin_map');
INSERT INTO group_permission (id, technical_name, name_key) VALUES (11, 'ADMIN_MOD', 'permission.admin_mod');
INSERT INTO group_permission (id, technical_name, name_key) VALUES (12, 'WRITE_MESSAGE', 'permission.write_message');
INSERT INTO group_permission (id, technical_name, name_key) VALUES (13, 'ADMIN_ACCOUNT_BAN', 'permission.admin_account_ban');
INSERT INTO group_permission (id, technical_name, name_key) VALUES (14, 'WRITE_MOD', 'permission.write_mod');
INSERT INTO group_permission (id, technical_name, name_key) VALUES (15, 'ADMIN_MODERATION_REPORT', 'permission.admin_moderation_report');
INSERT INTO group_permission (id, technical_name, name_key) VALUES (16, 'READ_ACCOUNT_PRIVATE_DETAILS', 'permission.read_account_private_details');
INSERT INTO group_permission (id, technical_name, name_key) VALUES (17, 'WRITE_MAP', 'permission.write_map');
INSERT INTO group_permission (id, technical_name, name_key) VALUES (18, 'READ_AUDIT_LOG', 'permission.read_audit_log');
INSERT INTO group_permission (id, technical_name, name_key) VALUES (19, 'WRITE_COOP_MISSION', 'permission.write_coop_mission');
INSERT INTO group_permission (id, technical_name, name_key) VALUES (20, 'ADMIN_CLAN', 'permission.admin_clan');
INSERT INTO group_permission (id, technical_name, name_key) VALUES (21, 'ADMIN_ACCOUNT_NOTE', 'permission.admin_account_note');
INSERT INTO group_permission (id, technical_name, name_key) VALUES (22, 'READ_TEAMKILL_REPORT', 'permission.read_teamkill_report');
INSERT INTO group_permission (id, technical_name, name_key) VALUES (23, 'WRITE_MATCHMAKER_POOL', 'permission.write_matchmaker_pool');

SELECT setval(pg_get_serial_sequence('group_permission', 'id'), max(id))
FROM group_permission;

DELETE FROM message where key IN (SELECT name_key from group_permission);
INSERT INTO message(key, language, region, value) SELECT name_key,'en', 'US', regexp_replace(name_key,'_',' ') from group_permission;

INSERT INTO user_group(id ,technical_name, name_key, public) VALUES (1, 'moderator', 'usergroup.moderator', TRUE);
INSERT INTO user_group(id ,technical_name, name_key, public) VALUES (2, 'moderator_private_details', 'usergroup.moderator_with_access_to_private_details' ,FALSE);
INSERT INTO user_group(id ,technical_name, name_key, public) VALUES (3, 'councilor', 'usergroup.councilor' ,TRUE);
INSERT INTO user_group(id ,technical_name, name_key, public) VALUES (4, 'admin', 'usergroup.administrator' ,FALSE);
INSERT INTO user_group(id ,technical_name, name_key, public) VALUES (5, 'mod_manager', 'usergroup.mod_manager' ,TRUE);
INSERT INTO user_group(id ,technical_name, name_key, public) VALUES (6, 'map_manager', 'usergroup.map_manager' ,TRUE);
INSERT INTO user_group(id ,technical_name, name_key, public) VALUES (7, 'tutorial_manager', 'usergroup.tutorial_manager' ,TRUE);
INSERT INTO user_group(id ,technical_name, name_key, public) VALUES (8, 'tournament_director', 'usergroup.tournament_director' ,TRUE);
INSERT INTO user_group(id ,technical_name, name_key, public) VALUES (9, 'translator', 'usergroup.translator' ,TRUE);

INSERT INTO message(key, language, region, value) SELECT name_key,'en', 'US', regexp_replace(name_key,'_',' ') from user_group;

SELECT setval(pg_get_serial_sequence('user_group', 'id'), max(id))
FROM user_group;

INSERT INTO group_permission_assignment(group_id, permission_id) VALUES (1,13); --Moderator - Admin Account Ban
INSERT INTO group_permission_assignment(group_id, permission_id) VALUES (1,21); --Moderator - Admin Account Note
INSERT INTO group_permission_assignment(group_id, permission_id) VALUES (1,15); --Moderator - Admin Moderation Report
INSERT INTO group_permission_assignment(group_id, permission_id) VALUES (1,22); --Moderator - Admin Read Teamkill Report
INSERT INTO group_permission_assignment(group_id, permission_id) VALUES (1,11); --Moderator - Admin Mod
INSERT INTO group_permission_assignment(group_id, permission_id) VALUES (1,10); --Moderator - Admin Map
INSERT INTO group_permission_assignment(group_id, permission_id) VALUES (1,1);  --Moderator - Admin Avatar
INSERT INTO group_permission_assignment(group_id, permission_id) VALUES (1,3);  --Moderator - Write email domain ban
INSERT INTO group_permission_assignment(group_id, permission_id) VALUES (2,16); --Moderator with access to private details - Read Account Private Details
INSERT INTO group_permission_assignment(group_id, permission_id) SELECT 4, id from group_permission; --ADMIN - ALL
INSERT INTO group_permission_assignment(group_id, permission_id) VALUES (3,4); --Councilor - Admin Vote
INSERT INTO group_permission_assignment(group_id, permission_id) VALUES (3,2); --Councilor - Admin Ladder Pool
INSERT INTO group_permission_assignment(group_id, permission_id) VALUES (3,8); --Councilor - Admin News Post
INSERT INTO group_permission_assignment(group_id, permission_id) VALUES (3,1); --Councilor - Admin Avatar
INSERT INTO group_permission_assignment(group_id, permission_id) VALUES (5,11); --Mod Manager - Admin Mod
INSERT INTO group_permission_assignment(group_id, permission_id) VALUES (6,10); --Map Manager - Admin Map
INSERT INTO group_permission_assignment(group_id, permission_id) VALUES (7,7); --Tutorial Manager - Admin Tutorial
INSERT INTO group_permission_assignment(group_id, permission_id) VALUES (8,8); --Tournament Director - Admin News Post
INSERT INTO group_permission_assignment(group_id, permission_id) VALUES (8,1); --Tournament Director - Admin Avatar
INSERT INTO group_permission_assignment(group_id, permission_id) VALUES (9,12); --Translator - Write Message