alter table ladder_map drop constraint ladder_map_featured_mod_id_fkey;
alter table ladder_map rename to matchmaker_map;

alter table matchmaker_map rename column featured_mod_id to matchmaker_pool_id;
alter table matchmaker_map alter column matchmaker_pool_id set not null;

alter table matchmaker_map
    add constraint matchmaker_map_matchmaker_pool_fk
        foreign key (matchmaker_pool_id) references matchmaker_pool (id);
