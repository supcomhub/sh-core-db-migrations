FROM boxfuse/flyway:5.2.4-alpine

ENV FLYWAY_EDITION community

COPY migrations /flyway/sql/

# Add test data to docker image, so you always have test data matching your version
COPY test-data.sql /test-data.sql
