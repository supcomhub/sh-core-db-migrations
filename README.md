# SupComHub core DB project

This project contains the SupComHub database structure for the core SupComHub database used by the API and server. 
Other databases like for the forums or wiki are not part of this repository.

You can [browse the schema of current `master` online](https://supcomhub.gitlab.io/sh-core-db-migrations/).

## Usage with sh-stack
We highly recommended using [sh-stack](https://github.com/SupComHub/sh-stack) to interact with the database.

### Preparing sh-stack
    git clone git@gitlab.com:supcomhub/sh-stack.git
    cd sh-stack
    cp .env.template .env
    cp -r config.template config

### Creating a new core database
    scripts/init-core-db.sh

### Updating the core database
In order to update an existing database to the newest schema version, execute:

    docker-compose run --rm sh-core-db-migrations migrate

### Connecting to the database
In order to connect to the database using the psql client, execute:

    docker exec -ti sh-core-db psql -U postgres

## Usage with plain Docker
**We highly advice you to start the db with sh-stack instead.**

If you do continue, you will lack the proper setup of database users and privileges. You also might run into 
compatibility issues with other services that are dependent on a specific db release. Please only continue, if you want 
to work on the db itself.

### Create a network
Create a network to connect the Docker containers to each other. _The `--link` method used in many tutorials is 
deprecated._

    docker network create sh

If you want other Docker containers to connect to the database, put them into the same network.

### Creating a new database
    docker run --network="al" --network-alias="sh-core-db" -p 5432:5432 \
               -e POSTGRES_PASSWORD=apple \
               -e POSTGRES_DB=sh \
               -e PGDATA=sh \
               -d --name sh-core-db \
               postgres:10.6-alpine

### Updating the database
In order to update an existing database to the newest schema version, first you need to build it from within the
directory:

    docker build -t sh-core-db-migrations .

Once the docker image was built you can start it:

    docker run --network="al" \
               -e FLYWAY_URL=jdbc:postgresql://sh-core-db/sh \
               -e FLYWAY_TABLE=schema_version \
               -e FLYWAY_USER=postgres \
               -e FLYWAY_PASSWORD=apple \
               sh-core-db-migrations migrate

## How to Contribute

To make changes to the database, add a new .sql file to the migrations folder. Each file needs to have a unique version
prefix and be one version higher than the latest one.

For more information on how the migration works please consult the [Flyway tutorial](https://flywaydb.org/getstarted/how).

Please also follow our [general contribution guidelines](https://github.com/SupComHub/db/wiki/How-to-Contribute).
