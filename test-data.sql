DELETE FROM game_review_summary;
DELETE FROM news_post;
DELETE FROM game_review;
DELETE FROM game_review_summary; -- deleting game_review triggers recalculation of game_review_summary

DELETE FROM mod_version_review_summary;
DELETE FROM mod_version_review;
DELETE FROM mod_version_review_summary;-- see above

DELETE FROM map_version_review_summary;
DELETE FROM map_version_review;
DELETE FROM map_version_review_summary;-- see above

DELETE FROM email_domain_ban;
DELETE FROM teamkill_report;
DELETE FROM account_verification;
DELETE FROM map_version_review;
DELETE FROM name_record;
DELETE FROM email_address;
DELETE FROM leaderboard_rating;
DELETE FROM matchmaker_map;
DELETE FROM game;
DELETE FROM map_version;
DELETE FROM map;
DELETE FROM social_relation;
DELETE FROM mod_version;
DELETE FROM mod;
DELETE FROM ban;
DELETE FROM clan_membership;
DELETE FROM clan;
DELETE FROM oauth_client;
DELETE FROM matchmaker_pool;
DELETE FROM leaderboard;
DELETE FROM featured_mod;
DELETE FROM user_group_membership;
DELETE FROM account;

CREATE EXTENSION IF NOT EXISTS "pgcrypto";
CREATE OR REPLACE FUNCTION password(bytea) returns text AS $$
SELECT concat('{sha256}', encode(digest($1, 'sha256'), 'hex'));
$$ LANGUAGE SQL STRICT IMMUTABLE;

-- Login table
-- Most accounts get a creation time in the past so that they pass account
-- age check.
insert into account (id,display_name,password) values (1,'test', password('test'));
insert into email_address(plain, distilled, account_id, "primary") values ('test@example.com', 'test@example.com', '1', true );
insert into account_verification(account_id, type, value) VALUES (1,'STEAM','1');

insert into account (id,display_name,password) values (2, 'Dostya', password('test'));
insert into email_address(plain, distilled, account_id, "primary") values ('dostya@cybran.example.com', 'dostya@cybran.example.com', '2', true );
insert into account_verification(account_id, type, value) VALUES (2,'TELEGRAM','2');
insert into account_verification(account_id, type, value) VALUES (2,'STEAM','3');

insert into account (id,display_name,password) values (3, 'Rhiza', password('test'));
insert into email_address(plain, distilled, account_id, "primary") values ('rhiza@aeon.example.com', 'rhiza@aeon.example.com', '3', false );
insert into account_verification(account_id, type, value) VALUES (3,'TELEGRAM','4');
insert into account_verification(account_id, type, value) VALUES (3,'STEAM','5');

insert into account (id,display_name,password) values ('4', 'NO_AUTH', password('test'));
insert into email_address(plain, distilled, account_id, "primary") values ('uid@uef.example.com', 'uid@uef.example.com', '4', true );

insert into account (id,display_name,password) values ('5', 'postman', password('test'));
insert into email_address(plain, distilled, account_id, "primary") values ('postman@postman.com', 'postman@postman.com', '5', true );
insert into account_verification(account_id, type, value) VALUES (5,'TELEGRAM','6');
-- Test IPv6
insert into account (id,display_name,password, last_login_ip_address) values ('6', 'ipv6', password('test'),'fe80::943d:7a29:18ab:485c%12');
insert into email_address(plain, distilled, account_id, "primary") values ('ipv6@example.com', 'ipv6@example.com', '6', true );
insert into account_verification(account_id, type, value) VALUES (6,'TELEGRAM','7');
insert into account_verification(account_id, type, value) VALUES (6,'STEAM','8');
-- Test Admin
insert into account (id,display_name,password, last_login_ip_address) values ('7', 'admin', password('test'),'127.0.0.1');
insert into email_address(plain, distilled, account_id, "primary") values ('admin@example.com', 'admin@example.com', '7', true );
insert into account_verification(account_id, type, value) VALUES (7,'TELEGRAM','9');
insert into account_verification(account_id, type, value) VALUES (7,'STEAM','10');
insert into user_group_membership(member_id, group_id) VALUES ('7',(SELECT id from user_group where technical_name='admin'));
-- Test Moderator
insert into account (id,display_name,password, last_login_ip_address) values ('8', 'moderator', password('test'),'127.0.0.1');
insert into email_address(plain, distilled, account_id, "primary") values ('mod@example.com', 'mod@example.com', '8', true );
insert into account_verification(account_id, type, value) VALUES (8,'TELEGRAM','11');
insert into account_verification(account_id, type, value) VALUES (8,'STEAM','12');
insert into user_group_membership(member_id, group_id) VALUES ('8',(SELECT id from user_group where technical_name='moderator'));

SELECT setval(pg_get_serial_sequence('account', 'id'), max(id))
FROM account;

-- Name history
insert into name_record (account_id, previous_name, new_name) values
  ('1', 'test', 'test_maniac'),
  ('2', 'Dostya', 'YoungDostya');

SELECT setval(pg_get_serial_sequence('name_record', 'id'), max(id))
FROM name_record;

-- Featured mods

insert into featured_mod (id, short_name, description, display_name, public, ordinal, git_url, git_branch, allow_override, current_version)
values ('1', 'standard', 'Standard', 'Standard', true, 1, 'https://github.com/FAForever/fa.git', 'deploy/faf', false, 1),
       ('2', 'ladder1v1', 'Ladder 1v1', 'Ladder 1v1', false, 0, 'https://github.com/FAForever/fa.git', 'deploy/faf', false, 1),
       ('3', 'coop', 'Cooperative', 'Cooperative', false, 0, 'https://github.com/FAForever/fa-coop.git', 'master', false, 1);

SELECT setval(pg_get_serial_sequence('featured_mod', 'id'), max(id))
FROM featured_mod;

-- Leaderboards

insert into leaderboard (id, technical_name, name_key, description_key)
values (1, 'global', 'leaderboard.global.name', 'leaderboard.global.description'),
       (2, 'ladder1v1', 'ladder1v1.global.name', 'ladder1v1.global.description');

SELECT setval(pg_get_serial_sequence('leaderboard', 'id'), max(id))
FROM leaderboard;

insert into leaderboard_rating (account_id, mean, deviation, total_games, won_games, leaderboard_id)
values
(1, 2000, 125, 10, 10, 1),
(2, 1500, 75, 10, 7, 1),
(3, 1650, 62.52, 10, 8, 1),
(4, 2000, 125, 10, 10, 2),
(5, 1500, 75, 10, 7, 2),
(6, 1650, 62.52, 10, 8, 2);

-- Matchmaker

insert into matchmaker_pool (id, featured_mod_id, leaderboard_id, name_key, technical_name)
values (1, 1, 1, 'matchmaker.global', 'global'),
       (2, 2, 2, 'matchmaker.ladder1v1', 'ladder1v1');

-- Sample maps
insert into map (id, display_name, map_type, battle_type, uploader_id)
values
('1', 'SCMP_001', 'FFA', 'skirmish', '1'),
('2', 'SCMP_002', 'FFA', 'skirmish', '1'),
('3', 'SCMP_003', 'FFA', 'skirmish', '1'),
('4', 'SCMP_004', 'FFA', 'skirmish', '1'),
('5', 'SCMP_005', 'FFA', 'skirmish', '1'),
('6', 'SCMP_006', 'FFA', 'skirmish', '2'),
('7', 'SCMP_007', 'FFA', 'skirmish', '2'),
('8', 'SCMP_008', 'FFA', 'skirmish', '2'),
('9', 'SCMP_009', 'FFA', 'skirmish', '2'),
('10', 'SCMP_010', 'FFA', 'skirmish', '3'),
('11', 'SCMP_011', 'FFA', 'skirmish', '3'),
('12', 'SCMP_012', 'FFA', 'skirmish', '3'),
('13', 'SCMP_013', 'FFA', 'skirmish', '3'),
('14', 'SCMP_014', 'FFA', 'skirmish', '3'),
('15', 'SCMP_015', 'FFA', 'skirmish', '3');

SELECT setval(pg_get_serial_sequence('map', 'id'), max(id))
FROM map;

insert into map_version (id, description, max_players, width, height, version, filename, hidden, map_id)
values
('1', 'SCMP 001', 8, 1024, 1024, 1, 'maps/scmp_001.zip', false, '1'),
('2', 'SCMP 002', 8, 1024, 1024, 1, 'maps/scmp_002.zip', false, '2'),
('3', 'SCMP 003', 8, 1024, 1024, 1, 'maps/scmp_003.zip', false, '3'),
('4', 'SCMP 004', 8, 1024, 1024, 1, 'maps/scmp_004.zip', false, '4'),
('5', 'SCMP 005', 8, 2048, 2048, 1, 'maps/scmp_005.zip', false, '5'),
('6', 'SCMP 006', 8, 1024, 1024, 1, 'maps/scmp_006.zip', false, '6'),
('7', 'SCMP 007', 8, 512, 512, 1, 'maps/scmp_007.zip', false, '7'),
('8', 'SCMP 008', 8, 1024, 1024, 1, 'maps/scmp_008.zip', false, '8'),
('9', 'SCMP 009', 8, 1024, 1024, 1, 'maps/scmp_009.zip', false, '9'),
('10', 'SCMP 010', 8, 1024, 1024, 1, 'maps/scmp_010.zip', false, '10'),
('11', 'SCMP 011', 8, 2048, 2048, 1, 'maps/scmp_011.zip', false, '11'),
('12', 'SCMP 012', 8, 256, 256, 1, 'maps/scmp_012.zip', false, '12'),
('13', 'SCMP 013', 8, 256, 256, 1, 'maps/scmp_013.zip', false, '13'),
('14', 'SCMP 014', 8, 1024, 1024, 1, 'maps/scmp_014.zip', false, '14'),
('15', 'SCMP 015', 8, 512, 512, 1, 'maps/scmp_015.zip', false, '15'),
('16', 'SCMP 015', 8, 512, 512, 2, 'maps/scmp_015.v0002.zip', false, '15'),
('17', 'SCMP 015', 8, 512, 512, 3, 'maps/scmp_015.v0003.zip', false, '15');

SELECT setval(pg_get_serial_sequence('map_version', 'id'), max(id))
FROM map_version;

insert into matchmaker_map (id, map_version_id, matchmaker_pool_id) values
('1', '1', '2'),
('2', '2', '2');

SELECT setval(pg_get_serial_sequence('matchmaker_map', 'id'), max(id))
FROM matchmaker_map;

insert into game (id, start_time, end_time, featured_mod_id, host_id, map_version_id, name, validity, victory_condition)
values ('1', NOW(), NOW(), '1', '1', '1', 'Test game','VALID','DEMORALIZATION');

SELECT setval(pg_get_serial_sequence('game', 'id'), max(id))
FROM game;

insert into social_relation ( from_id, to_id, relation)
values('1', '2', 'FRIEND'),
      ('2', '1', 'FOE');

insert into mod (id, display_name, author, uploader_id)
VALUES ('1', 'test-mod', 'baz', '1'),
       ('2', 'test-mod2', 'baz', '1'),
       ('3', 'test-mod3', 'baz', '1');

SELECT setval(pg_get_serial_sequence('mod', 'id'), max(id))
FROM mod;

insert into mod_version (id, type, description, version, filename, icon, mod_id, uuid) VALUES
        ('1', 'UI', 'desc', 1, 'foobar.zip', 'foobar.png', '1', '00000000-0000-0000-0000-000000000001'),
        ('2', 'SIM', 'desc', 1, 'foobar2.zip', 'foobar2.png', '2', '00000000-0000-0000-0000-000000000002'),
        ('3', 'UI', 'desc', 2, 'foobar3.zip', 'foobar3.png', '2', '00000000-0000-0000-0000-000000000003'),
        ('4', 'SIM', 'desc', 3, 'foobar4.zip', 'foobar4.png', '2', '00000000-0000-0000-0000-000000000004');

SELECT setval(pg_get_serial_sequence('mod_version', 'id'), max(id))
FROM mod_version;

-- sample bans
insert into ban(account_id, author_id, reason, scope) values
  ('2', '1', 'Test permanent ban', 'GLOBAL'),
  ('3', '1', 'This test ban should be revoked', 'CHAT');
insert into ban(account_id, author_id, reason, scope, expiry_time) values
  ('4', '1', 'This test ban should be expired', 'CHAT', NOW());
insert into ban (account_id, author_id, reason, scope, expiry_time, revocation_reason, revocation_author_id, revocation_time) values
  ('4', '1', 'This test ban should be revoked', 'CHAT', '2099-01-01', 'this was a test ban', '2', NOW());

-- sample clans
insert into clan (id, name, tag, founder_id, leader_id, description) values
  ('1', 'Alpha Clan', '123', '1', '1', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr'),
  ('2', 'Beta Clan', '345', '4', '4', 'Sed diam nonumy eirmod tempor invidunt ut labore'),
  ('3', 'Charlie Clan', '678', '2', '1', 'At vero eos et accusam et justo duo dolores et ea rebum');

SELECT setval(pg_get_serial_sequence('clan', 'id'), max(id))
FROM clan;

insert into clan_membership (clan_id, member_id) values
  ('1', '2'),
  ('1', '3'),
  ('2', '4'),
  ('3', '1');

-- sample oauth_client for Postman
insert into oauth_client ( name, client_secret, redirect_uris, default_redirect_uri, default_scope) VALUES
  ('Downlord''s FAF Client', '{noop}6035bd78-7730-11e5-8bcf-feff819cdc9f', '', '', 'read_events read_achievements upload_map'),
  ('faf-website', '{noop}banana', 'http://localhost:8020', 'http://localhost:8020', 'public_profile write_account_data create_user'),
  ('postman', '{noop}postman', 'http://localhost https://www.getpostman.com/oauth2/callback', 'https://www.getpostman.com/oauth2/callback', 'read_events read_achievements upload_map upload_mod write_account_data');

insert into teamkill_report (teamkiller_id, victim_id, game_id, game_time) VALUES ('1', '2', '1', 659);

insert into game_review (text, reviewer_id, score, game_id) VALUES ('Awesome', '1', 5, '1');
insert into game_review (text, reviewer_id, score, game_id) VALUES ('Nice', '2', 3, '1');
insert into game_review (text, reviewer_id, score, game_id) VALUES ('Meh', '3', 2, '1');

insert into map_version_review (text, reviewer_id, score, map_version_id) VALUES ('Fine', '1', 3, '1');
insert into map_version_review (text, reviewer_id, score, map_version_id) VALUES ('Horrible', '2', 1, '1');
insert into map_version_review (text, reviewer_id, score, map_version_id) VALUES ('Boah!', '3', 5, '1');

insert into mod_version_review (text, reviewer_id, score, mod_version_id) VALUES ('Great!', '1', 5, '1');
insert into mod_version_review (text, reviewer_id, score, mod_version_id) VALUES ('Like it', '2', 4, '1');
insert into mod_version_review (text, reviewer_id, score, mod_version_id) VALUES ('Funny', '3', 4, '1');

INSERT INTO email_domain_ban(domain) VALUES ('spam.org');

INSERT INTO news_post(author_id, headline, body, tags, pinned) VALUES ('7','Welcome to SH','This is markdown','welcome news',false);