#!/bin/sh

docker exec -i sh-core-db psql sh -v ON_ERROR_STOP=1 -U postgres < ./test-data.sql || { echo "Test data failed"; exit 1; };
# Run twice to check whether the cleanup works as well
docker exec -i sh-core-db psql sh -v ON_ERROR_STOP=1 -U postgres < ./test-data.sql || { echo "2nd test data failed"; exit 1; };
