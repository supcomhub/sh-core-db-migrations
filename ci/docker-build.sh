#!/bin/sh

CONTAINER_IMAGE=registry.gitlab.com/${CI_PROJECT_PATH}
IMAGE_VERSION=$([ -n "${CI_COMMIT_TAG}" ] && echo "${CI_COMMIT_TAG#*v}" || echo "${CI_COMMIT_REF_NAME##*/}")

docker build --cache-from "${CONTAINER_IMAGE}:latest" --tag "${CONTAINER_IMAGE}:${IMAGE_VERSION}" --tag "${CONTAINER_IMAGE}:latest" .
docker push "${CONTAINER_IMAGE}:latest"
if [ -n "${CI_COMMIT_TAG}" ]; then docker push "${CONTAINER_IMAGE}:${IMAGE_VERSION}"; fi
