#!/bin/sh

if [ -f ~/.pgpass ]; then
  echo "This script is not intended for use outside the docker container (file ~/.pgpass does not exist)"
  echo "Try instead: docker -i sh-core-db ${0}"
  exit 1
fi

pg_dump -U postgres sh || exit 1;
