#!/bin/sh
set -e

echo '# Build & Run Docker Container'
docker build -t sh-core-db-migrations .
docker network create sh
docker run --network="sh" --network-alias="sh-core-db" -p 5432:5432 \
           -e POSTGRES_PASSWORD=apple \
           -e POSTGRES_DB=sh \
           -e PGDATA=sh \
           -d --name sh-core-db \
           postgres:10.6-alpine

printf 'Waiting for sh-core-db'
counter=1
# Wait 2 minutes for docker container
while [ ${counter} -le 120 ]
do
    if docker exec -i sh-core-db pg_isready >/dev/null 2>&1; then

        # Run flyway migrations
        docker run --network="sh" \
                   -e FLYWAY_URL=jdbc:postgresql://sh-core-db/sh \
                   -e FLYWAY_USER=postgres \
                   -e FLYWAY_PASSWORD=apple \
                   sh-core-db-migrations migrate

        exit 0
    fi
    printf '.'
    sleep 1
    counter=$((counter+1))
done
echo
echo "Error: sh-core-db has not started after 2 minutes timeout"
echo "This is the container log:"
docker logs sh-core-db
exit 1
